import ROOT
import sys

if len(sys.argv) != 2:
    print(f"USAGE: {sys.argv[0]} <input file>")
    sys.exit(1)
inFileName = sys.argv[1]
inFile = ROOT.TFile.Open(inFileName, "READ")
tree = inFile.Get("Events")
# Print only first couple of branches
counter = 0
for branch in tree.GetListOfBranches():
    print(f"Branch: {branch}")
    counter += 1
    if counter > 30:
        break
print(f"Number of events: {tree.GetEntries()}")

